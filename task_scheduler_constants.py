USERNAME = '' # your username
PASSWORD = '' # your password

LOGGING_TIME = (23, 30) # first - hours, second - seconds (now it is 23:30)

TASK_TRIGGER_DAILY    = 2
TASK_ACTION_EXEC      = 0
TASK_LOGON_PASSWORD   = 1
TASK_CREATE_OR_UPDATE = 6

PYTHON_PATH  = '' # path to python (.exe file)
PROGRAM_PATH = '' # path to logging program

TASK_NAME   = 'Issues logging' # task   name in task scheduler
FOLDER_NAME = 'Issues logging' # folder name in task scheduler
# result will be \\FOLDER_NAME\TASK_NAME

STOP_IF_GOING_ON_BATTERIES     = False 
DISALLOW_START_IF_ON_BATTERIES = False
WAKE_TO_RUN = True
IS_ENABLED  = True

EXECUTION_TIME_LIMIT = "PT5M"  # 5 min for execute (if more - shutdown)

DESCRIPTION = "This daily task connects to gitlab and save issues information" # your description