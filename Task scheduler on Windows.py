from datetime import datetime, date, time
import win32com.client

from task_scheduler_constants import *

def Clear(issues_folder, root_folder):
    issues_folder.DeleteTask(TASK_NAME,   0)
    root_folder.DeleteFolder(FOLDER_NAME, 0)
    
def Print_Info(registred_task):
    print("Congratulations, you've created the task!")
    print("{:<11}{}".format("Task name",  " = " + registred_task.Name))
    print("{:<11}{}".format("Is enabled", " = " + str(registred_task.Enabled)))
    print("{:<11}{}".format("Next run",   " = " + str(registred_task.NextRunTime)))
    print("{:<11}{}".format("Path",       " = " +  registred_task.Path))
    
if __name__ == '__main__':
    scheduler = win32com.client.Dispatch('Schedule.Service')
    scheduler.Connect(user=USERNAME, password=PASSWORD) # connect to task scheduler
    
    root_folder = scheduler.GetFolder('\\')
    try:
        issues_folder = root_folder.CreateFolder(FOLDER_NAME) # create folder for our task
    except:
        issues_folder = root_folder.GetFolder(FOLDER_NAME)    # pick   folder for our task
        
    task_def = scheduler.NewTask(0)
    trigger = task_def.Triggers.Create(TASK_TRIGGER_DAILY) # daily trigger

    start_time = datetime.combine(datetime.now(), time(LOGGING_TIME[0], LOGGING_TIME[1])) # today date 23:30
    trigger.StartBoundary = start_time.isoformat()

    action = task_def.Actions.Create(TASK_ACTION_EXEC) 
    action.ID = 'IssuesLoggingId' # some id, doesn't matter for us
    action.Path      = PYTHON_PATH
    action.Arguments = PROGRAM_PATH

    task_def.RegistrationInfo.Description = DESCRIPTION

    task_def.Settings.Enabled                    = IS_ENABLED
    task_def.Settings.StopIfGoingOnBatteries     = STOP_IF_GOING_ON_BATTERIES
    task_def.Settings.DisallowStartIfOnBatteries = DISALLOW_START_IF_ON_BATTERIES 
    task_def.Settings.WakeToRun                  = WAKE_TO_RUN
    task_def.Settings.ExecutionTimeLimit         = EXECUTION_TIME_LIMIT

    task_def.Principal.LogonType = TASK_LOGON_PASSWORD # because we want to log on while computer is off

    registred_task = issues_folder.RegisterTaskDefinition( # task registration
        TASK_NAME, 
        task_def,
        TASK_CREATE_OR_UPDATE,
        USERNAME,     
        PASSWORD,     
        TASK_LOGON_PASSWORD)
    
    Print_Info(registred_task)

    # Clear(issues_folder, root_folder) # delete task and folder