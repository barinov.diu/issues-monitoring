from ProjectInfo import * # import class ProjectInfo and plot functions

if __name__ == "__main__":
    gl = gb.Gitlab(GITLAB_URL, ACCESS_TOKEN) # create Gitlab object
    
    gl.auth() # authentication in gitlab
    
    project = gl.projects.get(PROJECT_ID) # get your project
    
    project_info = ProjectInfo(project) # now you can call all the functions
    
    # everything above is as in the first example
    
    ''' 
    signature: 
        Clossed_X_Open_Issues(project_info, mode=SHOW_MODE)
        
    where:
        project_info - any ProjectInfo class
        
        mode - SHOW_MODE (default) or PDF_MODE, first draws a plot, second adds it to pdf
        
    briefly:
        illustrates the dependency between number of closed/open issues and date (first date is PROJECT_START_DATE)
    '''
 
    Closed_X_Open_Issues(project_info)